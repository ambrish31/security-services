package com.fusion.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class TestController
{

	@Autowired
	private Environment environment;

	@GetMapping("/get")
	public String get()
	{
		return "Profile active is :- " + " ----  " + Arrays.toString(environment.getActiveProfiles());
	}
}
